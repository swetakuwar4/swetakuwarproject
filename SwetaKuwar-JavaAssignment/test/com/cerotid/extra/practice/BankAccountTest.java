package com.cerotid.extra.practice;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.cerotid.bank.model.Account;

public class BankAccountTest {
	private Account account;

	@Before
	public void setUp() throws Exception {
		account = new Account();
		account.setAmountBalance(5000.00);
	}

	@Test
	public void testDeductAccountBalance() {
		account.deductAccountBalance(500.00);
		assertTrue(account.getAmountBalance()==4500.00);
	}

}
