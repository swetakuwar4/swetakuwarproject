package com.cerotid.extra.practice;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BankAccountTest1 {
	private BankAccount bankAccount;

	@Before
	public void setUp() throws Exception {
		bankAccount = new BankAccount();
		bankAccount.setAmountBalance(1000.00);
	}

	@Test
	public void testSendMoney() {
		bankAccount.deductAccountBalance(500.00);
		assertTrue(bankAccount.getAmountBalance()==4500.00);
	}

}
