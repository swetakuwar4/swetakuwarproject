package com.cerotid.first.assignment;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import com.cerotid.bank.model.Customer;


public class CustomerTest {
	private ArrayList<Customer> customers;

	@Before
	public void setUp() throws Exception {
		customers = new ArrayList<>();
		
		Customer cust1 = new Customer("David", "Smith" , "600-10-1234", "1995-10-02", "Ranipokhari");
		Customer cust2 = new Customer("Kim", "Smith" , "600-12-1234", "1990-10-02", "Baluwatar");
		Customer cust3 = new Customer("Sanchez", "K" , "600-15-1234", "1985-01-02", "Jhamsikhel");
		Customer cust4 = new Customer("Arnold", "Clark" , "600-10-999", "1995-10-02", "Jawalakhel");
		Customer cust5 = new Customer("Kevin", "Linn" , "499-10-1234", "1969-10-12", "New Road");
		
		customers.add(cust1);
		customers.add(cust2);
		customers.add(cust3);
		customers.add(cust4);
		customers.add(cust5);
	}

	@Test
		public void testSortCustomerBasedOnDOB() {
			Collections.sort(customers, Customer.DOBComparator);
			
			//Expected vs Actual
			//position 0 --> 1969-10-12
			
			assertEquals("1969-10-12", customers.get(0).getDOB());
			assertEquals("1985-01-02", customers.get(1).getDOB());
			assertEquals("1990-10-02", customers.get(2).getDOB());
			assertEquals("1995-10-02", customers.get(3).getDOB());
			assertEquals("1995-10-02", customers.get(4).getDOB());
		}
	}



