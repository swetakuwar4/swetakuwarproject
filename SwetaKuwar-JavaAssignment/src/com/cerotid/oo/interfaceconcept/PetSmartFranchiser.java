package com.cerotid.oo.interfaceconcept;

import com.cerotid.oo.principle.Animal;

public interface PetSmartFranchiser {
	public void registerAnimal(Animal animal);
	public Animal treatAnimal1(Animal animal);
	public Animal groomAnimal(Animal animal);

}
