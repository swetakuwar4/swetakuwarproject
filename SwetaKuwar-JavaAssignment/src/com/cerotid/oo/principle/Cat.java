package com.cerotid.oo.principle;

public class Cat extends PetAnimal {
	@Override
	public void live() {
		System.out.println("Wherever I like");
	}

	@Override
	public void sleepingTime() {
		System.out.println("I can sleep anytime ");
	}

}
