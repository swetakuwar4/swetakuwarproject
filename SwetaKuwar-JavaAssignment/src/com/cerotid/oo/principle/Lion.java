package com.cerotid.oo.principle;

public class Lion extends WildAnimal {
	@Override
	public void eat() {
		System.out.println("I eat Meat.");
		super.eat();
	}

	@Override
	public void live() {
		System.out.println("I live in Jungle and I am King");

	}

	@Override
	public void sleepingTime() {
		System.out.println("When I am full I want to Sleep ");
		
	}
}
