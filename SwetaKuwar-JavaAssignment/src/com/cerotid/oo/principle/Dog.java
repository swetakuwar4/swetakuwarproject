package com.cerotid.oo.principle;

public class Dog extends PetAnimal {
	// constractor(By default)
	Dog() {

	}

	// another type constructor is
	// overloaded constructor (which means same method is used in same class.)
	Dog(boolean isInsured) {
		//this.isIsured = isInsured;
		super.setIsured(isInsured);
		super.setNumberOfteeth(42);
	}
	Dog(boolean isSick ,String color){
		super.setSick(isSick);
		super.setColor(color);
	}

	@Override
	public void eat() {
		System.out.println("I eat Dog food.");
	}

	// e.g of overloading
	public void eat(String food) {
		System.out.println("I am eating" + food);

	}

	@Override
	public void sleepingTime() {
		System.out.println("I love to sleep when I want ");
		
	}
}
