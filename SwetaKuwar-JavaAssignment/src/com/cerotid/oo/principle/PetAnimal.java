package com.cerotid.oo.principle;
//abstract class effect because of extend of parentclass.
public abstract class PetAnimal extends Animal {
	protected boolean isIsured;
	
	public void live() {
		System.out.println("I live with people in their house");
	}

	//overriding
	public void eat() {
		System.out.println("I eat whatever people allow me to eat");
	}

	public boolean isIsured() {
		return isIsured;
	}

	public void setIsured(boolean isIsured) {
		this.isIsured = isIsured;
	}


}
