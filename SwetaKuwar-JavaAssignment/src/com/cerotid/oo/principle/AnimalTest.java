package com.cerotid.oo.principle;

import com.cerotid.oo.animalaction.Veterinary;

public abstract class AnimalTest {

	public static void main(String[] args) {
		// Creating Animal Object
		System.out.println("I am first animal");
		Animal animal1 = new Zebra();
		animal1.breathe();
		animal1.eat();
		animal1.makeSound();

		// create wildanimal obj
		System.out.println("I am second animal");
		WildAnimal wildanimal1 = new Lion();
		wildanimal1.breathe();
		wildanimal1.eat();
		wildanimal1.makeSound();
		wildanimal1.live();

		System.out.println("I am lion");
		Lion lion = new Lion();
		lion.eat();

		// We can assigned animal as dog (as same object)
		// Animal = Dog which is also an animal.
		Animal animal2 = new Dog();
		Animal animal3 = wildanimal1;

		Dog dog = new Dog(true, "Black");

		System.out.println("I am dog " + dog.isIsured() + " I have this many teeth " + dog.getNumberOfteeth());

		// Dog != Animal, because Al animal are not neccessary to be a dog.
		// Dog dog = new animal();

		Animal cowAnimal = new Cow();
		cowAnimal.eat();
		// override goes to close one
		Cow cow = new Cow();
		cow.eat();
		
		//create veterinary Obj
		Veterinary veterinary = new Veterinary();
		
		veterinary.treatAnimal(dog);
		veterinary.treatAnimal(cow);
		veterinary.treatAnimal(lion);
		

	}

}
