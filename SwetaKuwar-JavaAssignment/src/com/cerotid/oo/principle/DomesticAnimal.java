package com.cerotid.oo.principle;
//abstract class effect because of extend of parentclass.
public class DomesticAnimal extends Animal {
	
	public void live() {
		System.out.println("I live with people");
	}

	//overriding
	public void eat() {
		System.out.println("I eat whatever people allow me to eat");
	}
	
	//I know everyone sleeping method in parent class so, no need to define in subclass abstract 
	//just override for all class
	
	@Override
	public void sleepingTime() {
		System.out.println("My best job is to sleep");
		
	}


}
