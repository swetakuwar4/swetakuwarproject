package com.cerotid.oo.principle;
//abstract 
public abstract class Animal {
	private String color;
	private final int limbs = 4;
	private int numberOfteeth;
	private boolean isSick;

	//abstract means which we don't known actual answer, it's differ in everyclass.
	public abstract void sleepingTime(); 
	
	// final method means we cant override.
	public final void breathe() {
		System.out.println("Breathing in O2, breathin out Co2");
	}

	public void eat() {
		System.out.println("Eating Food");
	}

	public void makeSound() {
		System.out.println("I don't know which sound is produce");
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getNumberOfteeth() {
		return numberOfteeth;
	}

	public void setNumberOfteeth(int numberOfteeth) {
		this.numberOfteeth = numberOfteeth;
	}

	public int getLimbs() {
		return limbs;
		
		
	}

	public boolean isSick() {
		return isSick;
	}

	public void setSick(boolean isSick) {
		this.isSick = isSick;
	}

}
