
package com.cerotid.oo.principle;

public abstract class WildAnimal extends Animal {

	public void live() {
		System.out.println("I live in jungle");

	}

	// overriding means what ever method in parents class is same as child class.
	// overbuild /extends
	// overriding
	//public void eat() {
		//System.out.println("I eat whatever I found in jungle");
	//}

}
