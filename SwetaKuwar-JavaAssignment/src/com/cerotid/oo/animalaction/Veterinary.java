package com.cerotid.oo.animalaction;

import com.cerotid.oo.interfaceconcept.PetSmartFranchiser;
import com.cerotid.oo.principle.Animal;
import com.cerotid.oo.principle.Cat;
import com.cerotid.oo.principle.Cow;
import com.cerotid.oo.principle.Dog;
//Interface means implements for mulitlpe task
public class Veterinary implements PetSmartFranchiser{

	/*public Dog treatAnimal(Dog dog) {
		// TODO Treatment logic
		System.out.println("Dog is treaded ");
		return dog;
	}

	public Cat treatAnimal(Cat cat) {

		System.out.println("Cat is treaded ");
		return cat;
	}

	public Cow treatAnimal(Cow cow) {

		System.out.println("Cow is treaded ");
		return cow;
	}*/
	
	//ploymorphism means mulitple method in one method
	public Animal treatAnimal(Animal animal) {
		
		//instanceof
		if(animal instanceof Dog) {
			Dog dog = (Dog) animal;
			treatDog(dog);
			System.out.println("Dog is treated");
		} else if(animal instanceof Cow){
			System.out.println("Cow is treated");
			
		}else 
			System.out.println("Sorry" +animal.getClass().getSimpleName() + " is not treated");
			
	
		return animal;
	}

	private void treatDog(Dog dog) {
		explainAboutSick();
		Diagnose();
		
		if(dog.isSick()) {
			xray();
			inject();
			
		}
	}

	private void inject() {
		// TODO Auto-generated method stub
		
	}

	private void xray() {
		// TODO Auto-generated method stub
		
	}

	private void Diagnose() {
		// TODO Auto-generated method stub
		
	}

	private void explainAboutSick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerAnimal(Animal animal) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Animal treatAnimal1(Animal animal) {
		return animal;
		
		
	}

	@Override
	public Animal groomAnimal(Animal animal) {
		return animal;
		
	}

}
