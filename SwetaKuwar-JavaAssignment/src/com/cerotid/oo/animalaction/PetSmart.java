package com.cerotid.oo.animalaction;

import com.cerotid.oo.interfaceconcept.PetSmartFranchiser;
import com.cerotid.oo.interfaceconcept.VendingMachine;
import com.cerotid.oo.principle.Animal;

public class PetSmart implements PetSmartFranchiser {
	private VendingMachine vendingMachine;
	
	//constructor
	PetSmart(VendingMachine vendingMachine){
		this.vendingMachine = vendingMachine;
		
	}

	@Override
	public void registerAnimal(Animal animal) {
		System.out.println(animal.getClass().getSimpleName() +" Register petsmart way  ");

	}

	public void setVendingMachine(VendingMachine vendingMachine) {
		this.vendingMachine = vendingMachine;
	}

	public VendingMachine getVendingMachine() {
		return vendingMachine;
	}

	@Override
	public Animal treatAnimal1(Animal animal) {
		System.out.println(animal.getClass().getSimpleName() +" treated ");
		return animal;
	}

	@Override
	public Animal groomAnimal(Animal animal) {
		System.out.println(animal.getClass().getSimpleName() +" treated ");
		return animal;
	}

}
