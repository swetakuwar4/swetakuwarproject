package com.cerotid.oo.animalaction;

import com.cerotid.oo.interfaceconcept.CokeVendingMachine;
import com.cerotid.oo.interfaceconcept.EnergyDrinkVendingMachine;
import com.cerotid.oo.interfaceconcept.VendingMachine;

public class PetSmartTest {

	public static void main(String[] args) {
		VendingMachine vendingMachine = new CokeVendingMachine();

		PetSmart petsmart = new PetSmart(vendingMachine);
		petsmart.getVendingMachine().displayVendingMahine();

		VendingMachine energyDrink = new EnergyDrinkVendingMachine();
		petsmart.setVendingMachine(energyDrink);
		petsmart.getVendingMachine().displayVendingMahine();

	}

}
