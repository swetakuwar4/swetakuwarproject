package com.cerotid.extra.practice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import com.cerotid.bank.model.Customer;

public class InputCustomerTxt {

	private ArrayList<Customer> customerList = new ArrayList<>();
	private static String line;

	public static void readFile() throws FileNotFoundException {
		// Find the file and open the file

		Scanner scanner = new Scanner(new File("Customers.txt"));

		while (scanner.hasNextLine()) {
			System.out.println(scanner.nextLine());

			String line = scanner.nextLine();

			// From the above line of code we got a line from the file
			// content. Now we want to split the line with comma as the
			// character delimiter.
			Scanner lineScanner = new Scanner(line);
			lineScanner.useDelimiter(",");
			while (lineScanner.hasNext()) {
				// Get each splitted data from the Scanner object and print
				// the value.
				String part = lineScanner.next();
				System.out.print(part + ", ");
			}
			System.out.println();

			// ArrayList<String> list = new ArrayList<String>(Arrays.asList(line.split(",")));
			// System.out.println("LastName: "+ list.get(0));
			// System.out.println("FirstName: "+ list.get(1));
		}
		scanner.close();

	}

	public static void main(String[] args) throws FileNotFoundException {
		readFile();
	}
}
