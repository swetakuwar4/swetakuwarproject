package com.cerotid.extra.practice;

import java.util.Date;
import java.util.Random;
import java.util.Scanner;

import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.CheckType;
import com.cerotid.bank.model.DeliveryOptions;
import com.cerotid.bank.model.ElectronicCheckTransaction;
import com.cerotid.bank.model.MoneyGram;
import com.cerotid.bank.model.Transaction;
import com.cerotid.bank.model.WireTransfer;

public class BankAccount {


	public static int nextAccountNumber = 0;
	private AccountType accountType;
	private Date accountOpenDate;
	private Date accountCloseDate;
	private double amountBalance;
	private int accountNumber;

	public BankAccount() {
		this.accountOpenDate = accountOpenDate;
		this.accountCloseDate = null;
		this.accountNumber = new Random().nextInt();
		}

	public BankAccount(AccountType accountType, double amountBalance) {
			this.accountType = accountType;
			this.amountBalance = amountBalance;
			this.accountNumber = ++nextAccountNumber;
			this.amountBalance = 5000.0;

		}

	// Behaviors
	void printAccountInfo() {
		System.out.println(toString());
	}

	public void sendMoney() {
		double sendingAmount;
		int selectedTransaction = -1;
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the sending amount: ");
		sendingAmount = Double.parseDouble(input.nextLine());

		System.out.println("Please choose the type of transaction you want to use send money");
		System.out.println("Please choose 1 for WireTransfer , 2 for ElectornicCheck and 3 for MoneyGram");
		selectedTransaction = Integer.parseInt(input.nextLine());

		// Every if else should be in method, readability.

		if (selectedTransaction == 1) {
			String beneficiaryFirstName, beneficiaryLastName, intermediaryBankSWIFTCode, beneficiaryBankName,
					beneficiaryAccountNumber;
			System.out.println("Welcome to WireTransfer. Please provide information below :");
			System.out.println("beneficiaryFirstName ");
			beneficiaryFirstName = input.nextLine();
			System.out.println("BeneficiaryLastName ");
			beneficiaryLastName = input.nextLine();
			System.out.println("IntermediaryBankSWIFTCode");
			intermediaryBankSWIFTCode = input.nextLine();
			System.out.println("BeneficiaryBankName");
			beneficiaryBankName = input.nextLine();
			System.out.println("BeneficiaryAccountNumber");
			beneficiaryAccountNumber = input.nextLine();

		//	Transaction wireTransfer = new WireTransfer(this.accountNumber, sendingAmount, 10, beneficiaryFirstName,
			//		beneficiaryLastName, intermediaryBankSWIFTCode, beneficiaryBankName, beneficiaryAccountNumber);
			//wireTransfer.createTransaction();
			this.deductAccountBalance(sendingAmount + 10.00);
			System.out.println("Final Amount Left in Account : " + this.getAmountBalance());

		} else if (selectedTransaction == 2) {
			int checkOption;
			System.out.println("Welcome To Electronic Check.Please provide below information: ");
			System.out.println("Please provide the type of check wpuld you like to use");
			System.out.println("Enter 1 for Paper-Check and 2 for E-check");
			checkOption = input.nextInt();
			if (checkOption == 1) {
				Transaction electronicCheck = new ElectronicCheckTransaction(this.accountNumber, sendingAmount,CheckType.Paper_Check);
				electronicCheck.createTransaction();
				this.deductAccountBalance(sendingAmount + CheckType.Paper_Check.getFee());
				System.out.println("Final amount left in account: " + getAmountBalance());
			}
			else if (checkOption == 2) {
				Transaction electronicCheck = new ElectronicCheckTransaction(this.accountNumber, sendingAmount,CheckType.E_Check);
				electronicCheck.createTransaction();
				this.deductAccountBalance(sendingAmount + CheckType.E_Check.getFee());

			} else {
				System.out.println("Check type doesnot exit. Try Again");

			}
		} else if (selectedTransaction == 3) {
			int deliveryOption;
			String destinationCountry;
			System.out.println("Welcome to MoneyGram. Please provide below information");
			System.out.println("Please enter destination country: ");
			destinationCountry = input.nextLine();
			System.out.println("Please choose the delivery option. ");
			System.out.println("Enter 1 for 1-Minute and 2 for 24 HRS : ");
			deliveryOption = input.nextInt();
			
			if (deliveryOption == 1) {
				Transaction moneyGram = new MoneyGram(this.accountNumber, sendingAmount, DeliveryOptions.Ten_Minute,
						 destinationCountry);
				moneyGram.createTransaction();
				this.deductAccountBalance(sendingAmount + DeliveryOptions.Ten_Minute.getFee());
				System.out.println("Final amount left in account : " + this.getAmountBalance());
			}else if(deliveryOption == 2) {
				Transaction moneyGram = new MoneyGram(this.accountNumber, sendingAmount,DeliveryOptions.Twenty_Four_Hours,destinationCountry);
				moneyGram.createTransaction();
				this.deductAccountBalance(sendingAmount + DeliveryOptions.Twenty_Four_Hours.getFee());
				System.out.println("Final amount left in account : " + this.getAmountBalance());
			} else {
				System.out.println("Delivery Option doesn't exit.Try again.");
			}
		} else {
			System.out.println("Transfer method doesn't exit. Try again.");

		}
		input.close();
	}

	public void deductAccountBalance(double deductionAmount) {
		this.amountBalance -= deductionAmount;
	}

	// Access and Mutators
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public Date getAccountCloseDate() {
		return accountCloseDate;
	}

	public void setAccountCloseDate(Date accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}

	public double getAmountBalance() {
		return amountBalance;
	}

	public void setAmountBalance(double amountBalance) {
		this.amountBalance = amountBalance;
	}

	
	@Override
	public String toString() {
		return "Account [accountType=" + accountType + ", accountOpenDate=" + accountOpenDate + ", accountCloseDate="
				+ accountCloseDate + ", amountBalance=" + amountBalance + "]";

	}

}
