package com.cerotid.collection.concept;

import java.util.LinkedHashSet;
import java.util.TreeSet;

public class LinkedHashSetConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();

		linkedHashSet.add("Randy");
		linkedHashSet.add("Rosh");
		linkedHashSet.add("David");
		linkedHashSet.add("Dove");
		linkedHashSet.add("Sandy");

		/*
		 * //Can add null element as well set.add(null); set.add(null);
		 */
		System.out.println(linkedHashSet);

		// Converting LinkedHashSet to TreeSet

		TreeSet<String> tset = new TreeSet<>(linkedHashSet);
		System.out.println(tset);

	}

}
