package com.cerotid.collection.concept;

import java.util.HashSet;

public class HashSetConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashSet<String> set = new HashSet<String>();
		
		set.add("Randy");
		set.add("Rosh");
		set.add("David");
		set.add("Dove");
		set.add("Sandy");
		
		//Can add null element as well
		set.add(null);
		set.add(null);
		
		System.out.println(set);


	}

}
