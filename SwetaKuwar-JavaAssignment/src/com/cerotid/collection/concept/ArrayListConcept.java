package com.cerotid.collection.concept;

import java.util.ArrayList;

//ArrayList concept is store everything in one container.Fexibility
public class ArrayListConcept {

	public static void main(String[] args) {
		ArrayList aList = new ArrayList();

		aList.add("Dog");
		// aList.add(new Integer(2));
		aList.add(1);

		System.out.println(aList);

		Integer i = 0;
		String a = null;

		for (Object object : aList) {
			if (object instanceof String) {
				a = (String) object;
				System.out.println(a);
			} else if (object instanceof Integer)
			{
				i = (Integer) object;
				System.out.println(i);
			}

		}
		// System.out.println("You String " + a);
		// System.out.println("You String " + i);

	}

}
