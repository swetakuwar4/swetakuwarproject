package com.cerotid.collection.concept;

import java.util.LinkedList;

public class LinkedListConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<String> linkedList = new LinkedList<>();

		linkedList.add("John");
		linkedList.add("Don");
		linkedList.add("David");
		linkedList.add("Randy");
		linkedList.add("Rosa");
		linkedList.add("Sandy");

		System.out.println(linkedList);

		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());

		System.out.println(linkedList.poll());
	}

}
