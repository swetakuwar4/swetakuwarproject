/**
 * 
 */
package com.cerotid.bank.exception;

/**
 * @author bisha
 *
 */
public class AccountNotFoundException extends Exception {
	
	private static final long serialVersionUID = -8397888921462613834L;
	
	public AccountNotFoundException(String errorMessage) {
		// TODO Auto-generated constructor stub
		super(errorMessage);
	}

	/**
	 * 
	 */
	
	

}
