package com.cerotid.bank.exception;

public class InvalidTransactionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8945439867575927689L;

	public InvalidTransactionException(String errorMessage) {
		// TODO Auto-generated constructor stub
		super(errorMessage);
	}

}
