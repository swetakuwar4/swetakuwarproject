package com.cerotid.bank.model;

public class MoneyGram extends Transaction {
	// enum Delivery option
	private DeliveryOptions deliveryOptions;
	private String destinationCountry;
	
	

	public MoneyGram() {
		
		// TODO Auto-generated constructor stub
	}

	
	public MoneyGram(int accountNumber, double sendingAmount, DeliveryOptions tenMinute, String destinationCountry2) {
		// TODO Auto-generated constructor stub
	}


	public DeliveryOptions getDeliveryOptions() {
		return deliveryOptions;
	}

	public void setDeliveryOptions(DeliveryOptions deliveryOptions) {
		this.deliveryOptions = deliveryOptions;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

}
