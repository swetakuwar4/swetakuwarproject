package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Bank implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4430908056774152281L;
	private final String bankName = "ANC Bank";
	ArrayList<Customer> customers;

	// counstructor make to hold the customer list cointainer
	public Bank() {
		this.customers = new ArrayList<>();
	}

	// Required Accessor methods and Modifier method
	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public String getBankName() {
		return bankName;
	}

	// Behaviors
	public void printBankName() {
		System.out.println(getBankName());
	}

	public void printBankDetails() {
		//System.out.fromat("BankName= " + getBankName() + "customers= " + customers);
		System.out.println(toString());
		
	}

	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}

	// New Methods Assignment 5
	public void addCustomer(Customer customer) {
		customers.add(customer);
	}

	public void editCustomerCustomerInfo(Customer customer) {
		// TODO Add edit Customer logic
	}

	public Customer getCustomer(String ssn) {
		for (Customer customer : customers) {
			if (ssn.equals(customer.getSsn())) {
				return customer;
			}
		}

		return null;

	}
}
