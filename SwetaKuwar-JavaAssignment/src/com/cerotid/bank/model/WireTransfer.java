package com.cerotid.bank.model;

public class WireTransfer extends Transaction {
	//Variable
	private String beneficiaryFirstName;
	private String beneficiaryLastName;
	private String intermediaryBankSWIFTCode;
	private String beneficiaryBankName;
	private String beneficiaryAccountNumber;
	
//	public WireTransfer(int accountNumber, double sendingAmount, int i, String beneficiaryFirstName2,
//			String beneficiaryLastName2, String intermediaryBankSWIFTCode2, String beneficiaryBankName2,
//			String beneficiaryAccountNumber2) {
//		// TODO Auto-generated constructor stub
//	}
	
	
	public WireTransfer(String beneficiaryFirstName2, String beneficiaryLastName2, String intermediaryBankSWIFTCode2, String beneficiaryBankName2, String beneficiaryAccountNumber2) {
		
		// TODO Auto-generated constructor stub
	}


	//Access And  Mutators
	public String getBeneficiaryFirstName() {
		return beneficiaryFirstName;
	}
	public void setBeneficiaryFirstName(String beneficiaryFirstName) {
		this.beneficiaryFirstName = beneficiaryFirstName;
	}
	public String getBeneficiaryLastName() {
		return beneficiaryLastName;
	}
	public void setBeneficiaryLastName(String beneficiaryLastName) {
		this.beneficiaryLastName = beneficiaryLastName;
	}
	public String getIntermediaryBankSWIFTCode() {
		return intermediaryBankSWIFTCode;
	}
	public void setIntermediaryBankSWIFTCode(String intermediaryBankSWIFTCode) {
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
	}
	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}
	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}
	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}
	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}
	
	

}
