package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Customer implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1865680591719434532L;
	private String firstName;
	private String lastName;
	private ArrayList<Account> accounts;
	private String address;
	//ssn is sensitive so dont want save it back so key word (transient).when we diserialize back
	//private transient String ssn;
	private String ssn;
	private String DOB;

	// constructor
	public Customer() {

	}

	public Customer(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		
		if(this.accounts == null)
			this.accounts = new ArrayList<>();
	}
	public Customer(String fname, String lastName, String ssn, String address) {
		this.firstName = fname;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
		
		if(this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
	}
	
	public Customer(String fname, String lastName, String ssn, String dob, String address) {
		this.firstName = fname;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
		this.DOB = dob;
		
		if(this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
	}
	

	// Accessors and Mutators

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String adddress) {
		this.address = adddress;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		if(ssn.length() != 9) {
		this.ssn = null;
		return;
	}
	}
	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	// Behaviors
	public void printCustomersAccounts() {
		// TODO print customers account logic
				System.out.println();
	}
	
	public void printCustomersDetails() {
		System.out.println(toString());
	}
	
	

	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", accounts=" + accounts + ", address="
				+ address + ", ssn=" + ssn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		return true;
	}


	//comparator
	public static Comparator<Customer> LastNameComparator = new Comparator<Customer>() {

		@Override
		public int compare(Customer o1, Customer o2) {
			// TODO Auto-generated method stub
			if(o1.getLastName().compareTo(o2.getLastName()) ==0) {
				return o1.getFirstName().compareTo(o2.getFirstName());
			}
			return o1.getLastName().compareTo(o2.getLastName());

		}

	};
	
	public static Comparator<Customer> DOBComparator =
			new Comparator<Customer>() {

				@Override
				public int compare(Customer o1, Customer o2) {
					return o1.getDOB().compareTo(o2.getDOB());
				}
	};
	
	
	//add accounts
	public void addAccount(Account account) {
		this.accounts.add(account);
	
	}

}

