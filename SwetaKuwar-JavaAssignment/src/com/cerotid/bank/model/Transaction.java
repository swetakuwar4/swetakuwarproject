package com.cerotid.bank.model;

public class Transaction {
	// variable
	private double amount;
	private double fee;
	private String receiverFirstName;
	private String receiverLastName;
	private TransactionType transactiontype;
	
	public Transaction() {
		
	}

	public Transaction(double amount, double fee, String receiverFirstName, String receiverLastName,TransactionType transactionType) {
		this.amount = amount;
		this.fee = fee;
		this.receiverFirstName = receiverFirstName;
		this.receiverLastName = receiverLastName;
		this.transactiontype = transactionType;
	}

	// behaviour
	public void createTransaction() {

	}

	void deductAccountBalance() {

	}

	// Access and Mutator
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		 this.fee = fee;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public TransactionType getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(TransactionType transactiontype) {
		this.transactiontype = transactiontype;
	}

}
