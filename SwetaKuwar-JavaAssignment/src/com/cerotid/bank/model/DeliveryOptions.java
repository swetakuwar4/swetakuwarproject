package com.cerotid.bank.model;

public enum DeliveryOptions {
	Ten_Minute("10M"), Twenty_Four_Hours("24H");
	
	private double Fee;
	private final String time;

	DeliveryOptions(String time) {
		this.time = time;
	}

	public String getTime() {
		return time;
	}

	public double getFee() {
		return Fee;
	}

	public void setFee(double fee) {
		Fee = fee;
	}

	
}
