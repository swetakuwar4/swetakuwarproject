package com.cerotid.bank.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CustomerReader {

	private ArrayList<Customer> customerList = new ArrayList<>();

	public void readFile() throws IOException {
		// Find the file and open the file
		File file = new File("Customers.txt");

		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;

			// loop through line until end the file
		while ((line = bufferedReader.readLine()) != null) {
			
			ArrayList<String> list = new ArrayList<String>(Arrays.asList(line.split(",")));
			// Print the list
			//System.out.println("LastName: " + list.get(0));
			//System.out.println("FirstName: " + list.get(1));

			String lastName = list.get(0); 
			String firstName = list.get(1);
			  
			  Customer customer = new Customer(firstName, lastName);
			 customerList.add(customer);
			 

		}
		// close filereader
		bufferedReader.close();
		fileReader.close();
		System.out.println("Size of customerlist:" + customerList.size());

	}

	// Create file to write
	public void writeFile() throws IOException {
		// create the file
		File file = new File("CustomersOutput.txt");
		FileWriter filewriter = new FileWriter(file);
		
		// sort Customer
		sortCustomers();
		
		BufferedWriter bufferedWriter = new BufferedWriter(filewriter);
		
		for (Customer cust: customerList) {
			bufferedWriter.write(cust.getFirstName());
			bufferedWriter.write(" ");
			bufferedWriter.write(cust.getLastName());
			bufferedWriter.write("\n");
		}
		bufferedWriter.close();
		filewriter.close();

	}

	public void sortCustomers() {
		// sort by acendingorder
		 Collections.sort(customerList, Customer.LastNameComparator);
		 for (Customer cust: customerList) {
			 System.out.println(cust.getFirstName()+ " " + cust.getLastName());
			
		 }

	}

}
