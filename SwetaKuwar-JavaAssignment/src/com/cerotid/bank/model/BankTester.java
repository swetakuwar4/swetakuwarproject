package com.cerotid.bank.model;

import java.io.IOException;
import java.util.ArrayList;

public class BankTester {

	public static void main(String[] args) throws IOException {
		Bank bank = new Bank();
		// Bank inside (CustomerList and AccountList)
		// Customer1 all Info only.
		Account customer1Account1 = new Account();
		customer1Account1.setAccountType(AccountType.Checking);

		Account customer1Account2 = new Account();
		customer1Account2.setAccountType(AccountType.Saving);

		Account customer1Account3 = new Account();
		customer1Account3.setAccountType(AccountType.Bussiness_Checking);

		ArrayList<Account> customer1Accounts = new ArrayList<Account>();
		customer1Accounts.add(customer1Account1);
		customer1Accounts.add(customer1Account2);
		customer1Accounts.add(customer1Account3);

		Customer customer1 = new Customer();
		customer1.setAddress("123 Ector dr, TX, 12345");
		customer1.setFirstName("John");
		customer1.setLastName("Deo");
		customer1.setAccounts(customer1Accounts);
		// Customer2 info
		Customer customer2 = new Customer();
		Customer customer3 = new Customer();

		ArrayList<Customer> customerList = new ArrayList<Customer>();
		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);

		bank.setCustomers(customerList);

		bank.printBankName();
		bank.printBankDetails();
		
		CustomerReader crs = new CustomerReader();
		crs.readFile();
		crs.writeFile();
		
		
	}
}
