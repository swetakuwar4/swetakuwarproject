package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Scanner;

public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4914274966122588827L;
	public static int nextAccountNumber = 0;
	private AccountType accountType;
	private Date accountOpenDate;
	private Date accountCloseDate;
	private double amountBalance;
	private int accountNumber;

	public Account() {

	}

	public Account(AccountType accountType, Date accountOpenDate, double amountBalance) {
		this.accountType = accountType;
		this.accountOpenDate = accountOpenDate;
		this.amountBalance = amountBalance;
		this.setAccountNumber(++nextAccountNumber);
		

	}

	// Behaviors
	void printAccountInfo() {
		System.out.println(toString());
	}

	public void sendMoney() {
		
	}

	public void deductAccountBalance(double deductionAmount) {
		this.amountBalance -= deductionAmount;
	}

	// Access and Mutators
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public Date getAccountCloseDate() {
		return accountCloseDate;
	}

	public void setAccountCloseDate(Date accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}

	public double getAmountBalance() {
		return amountBalance;
	}

	public void setAmountBalance(double amountBalance) {
		this.amountBalance = amountBalance;
	}

	@Override
	public String toString() {
		return "Account [accountType=" + accountType + ", accountOpenDate=" + accountOpenDate + ", accountCloseDate="
				+ accountCloseDate + ", amountBalance=" + amountBalance + "]";
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

}
