package com.cerotid.bank.controller;

import java.util.List;

import com.cerotid.bank.exception.AccountNotFoundException;
import com.cerotid.bank.exception.InvalidTransactionException;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public interface BankingInterface {

	public void addCustomer(Customer customer);

	public boolean openAccount(Customer customer, Account account);

	public void sendMoney(Customer customer, Account account, Transaction transaction)
			throws AccountNotFoundException, InvalidTransactionException;

	public void depositMoneyInCustomerAccount(Customer customer,Account account);

	public void editCustomerInfo(Customer customer);

	public Customer getCustomerInfo(String ssn);
	
	public void printBankStatus();

	public void serializeBank();

	public void deserializeBank();
	

	public List<Customer> getCustomersByState(String StateCode);



}
