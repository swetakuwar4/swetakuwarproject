package com.cerotid.bank.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Scanner;

import com.cerotid.bank.exception.AccountNotFoundException;
import com.cerotid.bank.exception.InvalidTransactionException;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.CheckType;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.DeliveryOptions;
import com.cerotid.bank.model.Transaction;
import com.cerotid.bank.model.TransactionType;
import com.cerotid.bank.model.WireTransfer;

public class BankUI {

	private BankingInterface bankinginterface;

	private double amountBalance;

	public static void main(String[] args) {
		BankUI bankUI = new BankUI();
		bankUI.setBankingInterface(new BankController());
		bankUI.deserializeBank();

		while (true) {
			displayMainMenu();

			bankUI.performActionBasedOnMenuChoice();
		}
	}

	private static void displayMainMenu() {
		System.out.println("Choose from the menu below:");
		System.out.println("1. Add Customer");
		System.out.println("2. Add Account");
		System.out.println("3. Send Money");
		System.out.println("4. Print Bank Status");
		System.out.println("5. Exit");
	}

	private void performActionBasedOnMenuChoice() {
		while (true) {
			try {
				switch (getScannerInstance().nextInt()) {
				case 1:
					addCustomer();
					break;
				case 2:
					addCustomerAccount();
					break;
				case 3:
					sendMoney();
					break;
				case 4:
					bankinginterface.printBankStatus();

					break;
				case 5:
					SerializeBank();

					System.out.println("Thanks for visting out Bank!!");
					System.exit(0);
					break;
				default:
					System.out.println("Not in option!");
					break;
				}

				break; // breaks while loop when it execution point gets here
			} catch (Exception ex) {
				// Validation code
				System.out.println("Please provide valid integer input!");
			}
		}
	}

	private void sendMoney() throws AccountNotFoundException, InvalidTransactionException {
		Customer customer = getCustomer();
		// try {
		Account account = getAccount(customer);
		Transaction transaction = createTransation(customer, account);

		bankinginterface.sendMoney(customer, account, transaction);
//	} catch(AccountNotFoundException | InvalidTransactionException e) {
//		System.out.println("Error occurred while sending money :" + e.getMessage());
//		e.printStackTrace();
//	}

	}

	private Account getAccount(Customer customer) throws AccountNotFoundException {
		ArrayList<Account> accounts = customer.getAccounts();
		System.out.println("We found " + accounts.size() + "Accounts.");
		if (accounts.size() > 0) {
			displayAccountMenu(accounts);

			int customerInput = getCustomerInput(accounts);
			return accounts.get(customerInput - 1);
		} else {
			throw new AccountNotFoundException("Account doesn't exist,Please create account first");
		}

	}

	private int getCustomerInput(ArrayList<Account> accounts) {
		Scanner sc = getScannerInstance();
		Integer customerChoice = sc.nextInt();

		while (true) {
			if (customerChoice < 1 && customerChoice > accounts.size()) {
				System.out.println("Invalid choice. Please choose correct account.");
				displayAccountMenu(accounts);
				customerChoice = sc.nextInt();
			} else
				break;
		}
		return customerChoice;
	}

	private void displayAccountMenu(ArrayList<Account> accounts) {
		System.out.println("Please choose account to desired activity: ");

		for (Account account : accounts) {
			System.out.println((accounts.indexOf(account) + 1) + ". " + account.getAccountType());
		}

	}

	private Customer getCustomer() {

		return retrieveCustomerInformation();
	}

	private Transaction createTransation(Customer customer, Account account)
			throws AccountNotFoundException, InvalidTransactionException {
		Transaction transaction = new Transaction();

		if (customer != null) {
			TransactionType transactionType = getCustomerChoiceSendingMoneyType();

			double amount = getSendingAmount();

			transaction.setAmount(amount);
			transaction.setFee(amount * 0.1);
			transaction.setTransactiontype(transactionType);

			// balance = balance - (amount + (amount * transaction_Fee));

			// System.out.println(amount);
			// bankinginterface.sendMoney(customer, account, transaction);

		} else {
			System.out.println("Customer not available!");
		}
		return transaction;

	}

	// private void getFee(Transaction transaction) {
	// transaction.setFee(500);

	// }

	private double getSendingAmount() {
		System.out.println("Enter Amount which you want to sent: ");
		Scanner sendingAmount = getScannerInstance();

		return sendingAmount.nextDouble();
	}

	private void displaySendingMoneyChoice() {
		System.out.println("Choose Sending Money Type : [By default is ElectronicCheckTransfer ]: ");
		System.out.println("1. ElectronicCheckTransaction 2. WireTransfer 3. MoneyGram ");
	}

	private void addElectronicCheckTransactionType() {
		CheckType checkType;

		Scanner transactionInput = getScannerInstance();
		System.out.println("Choose ElectronicCheckTransactionType: 1. for PaperCheck 2. for E_Check");
		switch (transactionInput.nextInt()) {
		case 1:
			checkType = CheckType.Paper_Check;
			System.out.println("PaperCheck transfer");

			break;
		case 2:
			checkType = CheckType.E_Check;
			System.out.println("E_Check transfer");

		}
	}

	private void addWireTransferInfo() {
		Scanner WireTransferInfoInput = getScannerInstance();

		System.out.println("Welcome to WireTransfer. Please provide information below :");
		System.out.println("beneficiaryFirstName ");
		String beneficiaryFirstName = WireTransferInfoInput.nextLine();
		System.out.println("BeneficiaryLastName ");
		String beneficiaryLastName = WireTransferInfoInput.nextLine();
		System.out.println("IntermediaryBankSWIFTCode");
		String intermediaryBankSWIFTCode = WireTransferInfoInput.nextLine();
		System.out.println("BeneficiaryBankName");
		String beneficiaryBankName = WireTransferInfoInput.nextLine();
		System.out.println("BeneficiaryAccountNumber");
		String beneficiaryAccountNumber = WireTransferInfoInput.nextLine();

		WireTransfer wireTransfer = new WireTransfer(beneficiaryFirstName, beneficiaryLastName,
				intermediaryBankSWIFTCode, beneficiaryBankName, beneficiaryAccountNumber);
		wireTransfer.getAmount();

	}

	private void addMoneyGramType() {
		DeliveryOptions deliveryOption;
		Scanner transactionInput = getScannerInstance();
		System.out.println("Please select MoneyGramType:1. for Ten Mintues 2. for 24_HRS  ");
		switch (transactionInput.nextInt()) {
		case 1:
			deliveryOption = DeliveryOptions.Ten_Minute;
			System.out.println(" Please provide 10Min transfer  ");

			break;
		case 2:
			deliveryOption = DeliveryOptions.Twenty_Four_Hours;
			System.out.println("Please provide 24Hrs transfer ");

		}
	}

	private TransactionType getCustomerChoiceSendingMoneyType()
			throws AccountNotFoundException, InvalidTransactionException {
		displaySendingMoneyChoice();

		TransactionType transactionType = null;

		Scanner transactionTypeInput = getScannerInstance();
		switch (transactionTypeInput.nextInt()) {
		case 1:
			transactionType = TransactionType.ElectronicCheckTransaction;
			addElectronicCheckTransactionType();
			break;
		case 2:
			transactionType = TransactionType.WireTransfer;
			addWireTransferInfo();
			break;
		case 3:
			transactionType = TransactionType.MoneyGram;
			addMoneyGramType();
		default:
			transactionType = TransactionType.ElectronicCheckTransaction;
			break;
		}

		return transactionType;
	}

	private void addCustomerAccount() {

		Customer customer = retrieveCustomerInformation();

		if (customer != null) {

			AccountType accntType = getCustomerChoiceAccountTypeToOpen();

			double openingBalance = getOpeningBalance();

			Account account = new Account(accntType, new Date(0), openingBalance);

			customer.addAccount(account);

			bankinginterface.depositMoneyInCustomerAccount(customer, account);
		} else {
			System.out.println("Customer not available!");
		}
		bankinginterface.printBankStatus();
	}

	private double getOpeningBalance() {
		System.out.println("Provide Opening Balance in Account: ");
		Scanner accountBalance = getScannerInstance();

		return accountBalance.nextDouble();
	}

	private void displayAccountTypeChoice() {
		System.out.println("Choose AccountType: [By default is Checkign account]: ");
		System.out.println("1. Checking 2. Saving 3. Business Checking");
	}

	private AccountType getCustomerChoiceAccountTypeToOpen() {
		displayAccountTypeChoice();

		AccountType accountType = null;

		Scanner accountTypeInput = getScannerInstance();
		switch (accountTypeInput.nextInt()) {
		case 1:
			accountType = AccountType.Checking;
			break;
		case 2:
			accountType = AccountType.Saving;
			break;
		case 3:
			accountType = AccountType.Bussiness_Checking;
			break;

		default:
			accountType = AccountType.Checking;
			break;
		}
		return accountType;
	}

	private Customer retrieveCustomerInformation() {
		Scanner accountInput = getScannerInstance();

		System.out.println("Enter Customer's SSN to retrieve His Account Information: ");
		String ssn = accountInput.next();

		return bankinginterface.getCustomerInfo(ssn);
	}

	private void addCustomer() {
		Scanner customerInput = getScannerInstance();

		System.out.println("Enter Customer First Name: ");
		String firstName = customerInput.next();
		System.out.println("Enter Customer Last Name: ");
		String lastName = customerInput.next();
		System.out.println("Enter Customer 9 digit Social Security Number: ");
		String ssn = customerInput.next();
		System.out.println("Enter Customer Address:  ");
		// TODO
		String address = customerInput.next();

		Customer customer = new Customer(firstName, lastName, ssn, address);

		bankinginterface.addCustomer(customer);
		bankinginterface.getCustomerInfo(ssn);

		bankinginterface.printBankStatus();

	}

	private static Scanner getScannerInstance() {
		return new Scanner(System.in);
	}

	public BankingInterface getBankingInterface() {
		return bankinginterface;
	}

	public void setBankingInterface(BankingInterface bankingInterface) {
		this.bankinginterface = bankingInterface;
	}

	public void SerializeBank() {
		bankinginterface.serializeBank();
	}

	public void deserializeBank() {
		bankinginterface.deserializeBank();

	}

	public double getAmountBalance() {
		amountBalance = +amountBalance;
		return amountBalance;
	}

	public void setAmountBalance(double amountBalance) {
		this.amountBalance = amountBalance;
		// amountBalance =+ amountBalance;
	}

}
