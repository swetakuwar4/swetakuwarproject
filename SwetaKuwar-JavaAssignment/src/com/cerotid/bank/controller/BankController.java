package com.cerotid.bank.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.cerotid.bank.exception.AccountNotFoundException;
import com.cerotid.bank.exception.InvalidTransactionException;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;
import com.cerotid.bank.model.TransactionType;
import com.cerotid.bank.model.WireTransfer;

public class BankController implements BankingInterface {

	private static Bank bank;

	static {
		bank = new Bank();

	}

	public BankController() {
		super();

	}

	@Override
	public void addCustomer(Customer customer) {
		bank.getCustomers().add(customer);

	}

	@Override
	public boolean openAccount(Customer customer, Account account) {
		// check the customer exit in the bank
		if (bank.getCustomers().contains(customer)) {
			// find the location and add account
			int i = bank.getCustomers().indexOf(customer);

			bank.getCustomers().get(i).getAccounts();
			return true;
		} else {
			System.out.println("Customer doesn't exit");
			return false;
		}
	}

	@Override
	public void sendMoney(Customer customer, Account account, Transaction transaction)
			throws AccountNotFoundException, InvalidTransactionException {

		System.out.println("send money call");

		if (transaction.getTransactiontype() == TransactionType.ElectronicCheckTransaction) {
			double fee = transaction.getFee();
			double amountToBeSent = transaction.getAmount();

			if (checkIfAccountHasEnoughFund(fee, amountToBeSent, account)) {
				adjustCustomerAccount(customer, account, fee, amountToBeSent);
				System.out.println("Remaining Balance" + account.getAmountBalance());

			} else
				throw new InvalidTransactionException("Enough fund is not avialable for customer" + customer);

		} else if (transaction.getTransactiontype() == TransactionType.WireTransfer) {
			// TODO WireTrasfer related activities
			double fee = transaction.getFee();
			double amountToBeSent = transaction.getAmount();

//		if (!wireTransferBusinessRulesValidation((WireTransfer) transaction)) {
//			throw new InvalidTransactionException("Business rules didn't for this transaction");
//		}

			// Check if Account is eligible for wireTransfer
			// Check if Account has enough fund
			if (checkIfAccountHasEnoughFund(fee, amountToBeSent, account)) {
				adjustCustomerAccount(customer, account, fee, amountToBeSent);
				System.out.println("Remaining Balance" + account.getAmountBalance());
			} else
				throw new InvalidTransactionException("Enough fund is not avialable for customer" + customer);

		} else if (transaction.getTransactiontype() == TransactionType.MoneyGram) {
			double fee = transaction.getFee();
			double amountToBeSent = transaction.getAmount();

			if (checkIfAccountHasEnoughFund(fee, amountToBeSent, account)) {
				adjustCustomerAccount(customer, account, fee, amountToBeSent);
				System.out.println("Remaining Balance" + account.getAmountBalance());
			} else
				throw new InvalidTransactionException("Enough fund is not avialable for customer" + customer);
		}

	}

	private boolean wireTransferBusinessRulesValidation(WireTransfer transaction) throws InvalidTransactionException {
		// TODO if amount>100000

		if (transaction.getAmount() > 100000) {
			throw new InvalidTransactionException("Transaction over 100000 is not allowed");
		}

		return true;

	}

	private void adjustCustomerAccount(Customer customer, Account account, double fee, double amountToBeSent)
			throws AccountNotFoundException {
		// Customer--> 5 accounts
		int index = customer.getAccounts().indexOf(account);

		if (index != -1) {
			Account acc = customer.getAccounts().get(index);

			acc.deductAccountBalance(fee + amountToBeSent);
		} else {
			throw new AccountNotFoundException("Account doesn't exist for the customer:" + customer);
		}

	}

	private boolean checkIfAccountHasEnoughFund(double fee, double amountToBeSent, Account account) {

		if (account.getAmountBalance() >= (fee + amountToBeSent)) {
			return true;
		}

		return false;
	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer, Account account) {
		// TODO need to what type of deposit is and add in it?
		// TODO add amount is there account type as per customer want again and again?

		if (account.getAccountType() == AccountType.Checking) {
			double amount = account.getAmountBalance();

			int index = customer.getAccounts().indexOf(account);

			if (index != -1) {
				Account acc = customer.getAccounts().get(index);
				acc.getAmountBalance();
				// System.out.println("Total Balance of checking account: " + amount);

			} else if (account.getAccountType() == AccountType.Saving) {

			} else if (account.getAccountType() == AccountType.Bussiness_Checking) {

			}
		}
	}

	@Override
	public void editCustomerInfo(Customer customer) {

	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		if (ssn.length() != 9) {
			System.out.println("Please provide vaid input");
			return null;
		} else
			return bank.getCustomer(ssn);

	}

	@Override
	public void printBankStatus() {
		System.out.println(bank);

	}

	@Override
	public void serializeBank() {

		try {
			FileOutputStream fileOut = new FileOutputStream("bankinfo.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(bank);

			out.close();
			fileOut.close();

			System.out.printf("Serialized data is saved as bank.ser");
		} catch (IOException i) {
			i.printStackTrace();
		}

	}

	@Override
	public List<Customer> getCustomersByState(String StateCode) {

		return null;
	}

	@Override
	public void deserializeBank() {

		try {
			FileInputStream fileIn = new FileInputStream("bankinfo.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bank = (Bank) in.readObject();

			in.close();
			fileIn.close();

			System.out.println(bank);

		} catch (FileNotFoundException i) {
			System.out.println(" Welcome Back ");
			i.printStackTrace();

		} catch (IOException i) {
			System.out.println("Welcome to Bank");
			i.printStackTrace();

		} catch (ClassNotFoundException c) {
			System.out.println("customer not found");
			c.printStackTrace();

		} catch (Exception e1) {
			e1.printStackTrace();
		}

		System.out.println(bank.getBankName());

	}

}
