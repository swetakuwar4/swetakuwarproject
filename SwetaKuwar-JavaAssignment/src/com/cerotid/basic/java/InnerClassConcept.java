package com.cerotid.basic.java;


public class InnerClassConcept {
	
	public static void main(String[] args) {
		InnerClassConcept inc = new InnerClassConcept();
		
		//Instantiate class
		InnerB innerCLassobject = inc.new InnerB();
		innerCLassobject.accessInnerPropertyofB();
		
		InnerA.accessInnerProperty();
	}
	
	private static class InnerA{
		public static void accessInnerProperty() {
			
		}
		
	}
	
	private class InnerB{
		public void accessInnerPropertyofB() {
			
		}
		
	}
}
