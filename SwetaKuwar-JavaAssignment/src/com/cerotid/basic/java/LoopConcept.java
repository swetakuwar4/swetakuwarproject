package com.cerotid.basic.java;

public class LoopConcept {

	public static void main(String[] args) {
		
		System.out.println("John Deo");
		System.out.println("John Deo");
		System.out.println("John Deo");
		//but we use loop condition instead of paste.
		//Loop is use for repeating same things time to time
		
		//Types of loop
		//For loop
		//while loop
		//Do while loop
		//for each
		
		//for loop using variable.
		//0,1,2,3,4
		for(int i = 0; i < 5; i++) {
			System.out.println("Name = John Deo");
		}
		//While loop
		int j = 0;
		while(j<5) {
			System.out.println("I am in java class");
			
			j++; //j =j+1
		}
		//do while
		
		int k = 0;
		do {
			System.out.println("I am learning");
			 k++; //k = k+1
		}while(k < 5);
		
	}

}
