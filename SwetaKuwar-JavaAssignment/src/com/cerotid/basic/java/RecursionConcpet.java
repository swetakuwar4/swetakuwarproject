package com.cerotid.basic.java;

public class RecursionConcpet {
	// calculate factorial
	// !3= 3*2*1 = 6; !5 = 5*4*3*2*1=120
	public static int factorial(int number) {
		if (number == 0)
			return 1;
		else
			return number * factorial(number - 1);

		// 5*factorial(5)==5*24 =120
		// 4*factorial(4)==4*6 =24
		// 3*factorial(3)==3*2 =6
		// 2*factorial(2)==2*1 =2
		// 1*factorial(0)==1*1 =1
		// 1 start from down
	}

	public static int calculateFactorialUsingLoop(int num) {
		int result = num;

		for (int i = num -1; i > 0; i--) {
			result = result * i;
		}
		return result;

	}

	public static void main(String[] args) {
		System.out.println(RecursionConcpet.calculateFactorialUsingLoop(5));
		System.out.println(RecursionConcpet.factorial(5));

	}

}
