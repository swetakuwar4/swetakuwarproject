package com.cerotid.basic.java;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOConcept {

	public static void main(String[] args) throws IOException {
		// To read the files.
		FileReader reader = null;
		// To write the files.
		FileWriter writer = null;

		reader = new FileReader("inputText.txt");
		writer = new FileWriter("outputText.txt");

		int character;
		while ((character = reader.read()) != -1) {
			System.out.println(character);
			writer.write(character);
		}
		reader.close();
		writer.close();

	}

}
