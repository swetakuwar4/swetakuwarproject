package com.cerotid.basic.java;

public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EncapsulationConcept enc = new EncapsulationConcept();

		// enc.name = "John";
		enc.setName("John");

		// enc.ssn = "111-222-333311";
		// enc.setSsn("11223334511");
		enc.setSsn("123456789");

		System.out.println("Name " + enc.getName());
		enc.printEmployeeInfo();

	}

}
