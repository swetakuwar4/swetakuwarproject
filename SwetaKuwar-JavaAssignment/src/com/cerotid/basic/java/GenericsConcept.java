package com.cerotid.basic.java;

import java.util.ArrayList;

public class GenericsConcept {

	public static void main(String[] args) {
		String name1 = "Randy";
		String name2 = "candy";
		String name3 = "Mandy";

		int age1 = 12;
		int age2 = 20;
		int age3 = 22;
		
		//any kind of datatype in arrryList
		ArrayList trashCan = new ArrayList();
		trashCan.add(name1);
		trashCan.add(name2);
		trashCan.add(name3);
		
		trashCan.add(age1);
		trashCan.add(age2);
		trashCan.add(age3);
		
		//in generices datatype is define. If datatype is string it's only take string. 
		ArrayList<String> stringCan = new ArrayList<String>();
		stringCan.add(name1);
		stringCan.add(name2);
		stringCan.add(name3);
		
		//stringCan.add(age1);//eg.not take int data type 
		//stringCan.add(age2);
		//stringCan.add(age3);
		
		
		

	}

}
