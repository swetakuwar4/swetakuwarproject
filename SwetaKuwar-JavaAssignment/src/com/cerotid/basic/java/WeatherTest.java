package com.cerotid.basic.java;

public class WeatherTest {
	public static void main(String[] args) {
		System.out.println(Weather.SPRING);

		Weather weather = Weather.SPRING;

		switch (weather) {
		case SUMMER:
			System.out.println("Stay home");
			break;
		case WINTER:
			System.out.println("Stay wram");
			break;
		case FALL:
			System.out.println("Enjoy color");
			break;
		case SPRING:
			System.out.println("Have party");
			break;
		default:
			break;
		}

		System.out.println(Weather.SUMMER.getWeather());
	}

}
